import fromEvent from 'xstream/extra/fromEvent'
import { Stream } from 'xstream'
import isMobile from 'ismobilejs'

const isMobileDevice = new isMobile.Class(window.navigator.userAgent).any

export interface WindowSize {
	isBase: boolean
	gtBase: boolean
	isMedium: boolean
	gtMedium: boolean
	isLarge: boolean
	gtLarge: boolean
	is_X_Large: boolean
	gt_X_Large: boolean
	is_X_X_Large: boolean
	width: number
  height: number
  isMobileDevice: boolean
}

function windowSize () {
  return {
    width: window.innerWidth,
    height: window.innerHeight
  }
}

function windowSizeDriver(): Stream<WindowSize> {
  return fromEvent(window, 'resize')
    .map(windowSize)
    .startWith(windowSize())
		.remember()
		.map(({width, height}) => ({
			isBase: width < 547,
			gtBase: width >= 547,
			isMedium: width >= 547 && width < 767,
			gtMedium: width >= 767,
			isLarge: width >= 767 && width < 1024,
			gtLarge: width >= 1024,
			is_X_Large: width >= 1024 && width < 1200,
			gt_X_Large: width >= 1024,
			is_X_X_Large: width >= 1200,
			width,
      height,
      isMobileDevice,
    }))
}

export default windowSizeDriver
