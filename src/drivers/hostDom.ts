import Actions, { HostDomAction } from './hostDom.actions'

function makeHostDomDriver() {
	// a write only driver
	return (msg$: any) => {
		msg$.addListener({
			next: (msg: HostDomAction) => {
        let element: HTMLElement | null // may not be found

				switch (msg.type) {
					case Actions.DISABLE_SCROLLING:
            element = document.querySelector('body')

            if (element) {
              const foundElement: HTMLElement = element
              element.style.overflowX = 'hidden'
						  element.style.overflowY = 'hidden'
            }

						break

					case Actions.ENABLE_SCROLLING:
            element = document.querySelector('body')

            if (element) {
              const foundElement: HTMLElement = element
              element.style.overflowX = 'initial'
              element.style.overflowY = 'initial'
            }

						break

					case Actions.SET_PAGE_TITLE:
						document.title = msg.payload
            break

          case Actions.SET_ELEMENT_PROPERTY:
					  msg.payload.element[msg.payload.property] = msg.payload.value
					  break

					default:
						break
				}
			}
		})
	}
}

export default makeHostDomDriver
