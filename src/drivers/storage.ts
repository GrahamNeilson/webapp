import {Stream} from 'xstream'
import {adapt} from '@cycle/run/lib/adapt'

export interface IStorage {
	key: string
	value: any
}

export function makeStorageDriver() {
	const storageDriver =
		(outgoing$: Stream<IStorage>) => {
			outgoing$.addListener({
				next: (outgoing: IStorage)  => {
					localStorage[outgoing.key] = outgoing.value
				},
			})

			const incoming$: Stream<Storage> = Stream.create({
				start: listener => {
					listener.next(localStorage)
				},
				stop: () => {},
			})

			return adapt(incoming$)
		}

	return storageDriver
}
