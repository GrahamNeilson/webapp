export interface HostDomAction {
  type: string;
  payload?: any;
}

const DISABLE_SCROLLING = 'DISABLE_SCROLLING'
const ENABLE_SCROLLING = 'ENABLE_SCROLLING'
const SET_PAGE_TITLE = 'SET_PAGE_TITLE'
const SET_ELEMENT_PROPERTY = 'SET_ELEMENT_PROPERTY'

const actions = {
	DISABLE_SCROLLING,
	ENABLE_SCROLLING,
  SET_PAGE_TITLE,
  SET_ELEMENT_PROPERTY,
}

export default actions
