import { run } from '@cycle/run'
import { makeDOMDriver, div } from '@cycle/dom'
import windowSizeDriver from './drivers/windowSize'
import makeHostDomDriver from './drivers/hostDom'
import { makeHistoryDriver } from '@cycle/history'
import {makeStorageDriver} from './drivers/storage'
import onionify from 'cycle-onionify'

import App from './app/'
import Content from './app/content'
// import Footer from './app/footer'
// import Head from './app/head'
// import NavBar from './app/navBar'
import Carousel from './app/content/carousel' // running this standalone shows a CSS issue, i think / maybe / discuss
import './scss/styles.scss'
import xs from 'xstream'

const drivers = {
	WindowSize: windowSizeDriver,
  DOM: makeDOMDriver('#app'),
  History: makeHistoryDriver(),
	HostDomAction: makeHostDomDriver(),
	Storage: makeStorageDriver(),
	wrappedContent$: () => xs.of(div('Carousel requires a wrappedContent source')),
}

const wrappedMain = onionify(App) // App has state as Content needs state (Carousel)
// const wrappedMain = onionify(Content) // Content has state so needs to run in an onionified app too.
// const wrappedMain = onionify(Carousel) // App has state as Content needs state (Carousel)

run(wrappedMain, drivers)
// run(Footer, drivers)
// run(Head, drivers)
// run(NavBar, drivers)

