export interface IMenuItem {
	key: string,
	href: string,
	title: string,
}

export interface IPrimaryMenuItem extends IMenuItem {}
export interface ISecondaryMenuItem extends IMenuItem {}

export const primaryMenuItems: Array<IPrimaryMenuItem> = [{
	key: 'scommesse',
	href: 'http://www.skybet.it/',
	title: 'Scommesse',
}, {
	key: 'casino',
	href: 'http://casino.skybet.it/',
	title: 'Casinò',
}]

export const secondaryMenuItems: Array<ISecondaryMenuItem> = [{
	key: 'home',
	href: '/.',
	title: 'Home',
}, {
	key: 'jackpot',
	href: '/slots/jackpot',
	title: 'Jackpot',
}, {
	key: 'slots',
	href: '/slots',
	title: 'Slots',
}, {
	key: 'giochi-live',
	href: '/live',
	title: 'Giochi Live',
}, {
	key: 'volley',
	href: '/table_&_card_games',
	title: 'Giochi da Tavolo',
}]
