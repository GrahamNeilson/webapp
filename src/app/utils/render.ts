import { li, a, VNode, div, ul, img, p, address, h2, h1 } from '@cycle/dom'

export interface Link {
	href: string
	title: string
	target?: string
	rel?: string
	selected?: boolean
}

export interface Image {
	src: string
	alt?: string
}

export function renderList(dom: VNode[]): VNode {
	return ul('.list', dom)
}

export function renderListItem(dom: VNode): VNode {
	return li('.listItem', dom)
}

export function renderLink({href, title, target, rel}: Link, classes = []): VNode {
	return a(`.link ${classes.join(' ')}`, {
		attrs: {
			href,
			title,
			target: !!target ? target : undefined,
			rel: !!rel ? rel : undefined,
		},
	}, title)
}

export function renderImage({src, alt}: Image, classes = []): VNode {
	return img(`.image ${classes.join(' ')}`, {
		attrs: {
			src,
			alt: !!alt ? alt : undefined,
		},
	})
}

export function renderImageLink({href, title, src, target}: any) {
  return a('.link .imageLink', {
		attrs: {
			href,
			title,
			target: !!target ? target : undefined
		}
	}, renderImage({src, alt: title}))
}

export function renderAddress(addressData: string) {
	return address('.address',
		p(addressData)
	)
}
export const renderHeader2 = (text: string) => renderHeader(text, h2)

export function renderHeader(text: string, tag: any = h1) {
	return div('.header',
		tag('.heading', text)
	)
}
