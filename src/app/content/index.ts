import xs, { Stream } from 'xstream'
import { div, VNode, DOMSource } from '@cycle/dom'
import isolate from '@cycle/isolate'

import Carousel from './carousel'
import {State as CarouselState} from './carousel'
import ExampleContent from './exampleContent'

import { StateSource, Reducer } from 'cycle-onionify'
import { Location } from 'history'
import { WindowSize } from '../../drivers/windowSize'


interface State {
  carousel: CarouselState
}

interface Sinks {
	DOM: Stream<VNode>
	onion: Stream<Reducer<State>>
}

interface Sources {
  DOM: DOMSource
  History: Stream<Location>
	onion: StateSource<State>
	WindowSize: Stream<WindowSize>
}

function Content(sources: Sources): Sinks {
  const ExampleComponent = ExampleContent()
  const exampleComponentDom$ = ExampleComponent.DOM

	// we're putting the child content on the state so the carousel can access it easily, and
	// in a manner that means we can use the state$ source, rather than a new stream of content source.
	// this keeps the number of source types down, for on, which tend to be required from main
	// entry point down
  const defaultReducer$: Stream<Reducer<State>> =
    exampleComponentDom$.map((exampleComponentDom: VNode) => {
      return function() {
        return {
          carousel: {
            content: exampleComponentDom,
            slideIndex: 0,
          }
        }
      }
    })

  // we are using a Lens to set the carousel state.
  // this is because of the shape of our state in both components.
  // the pattern I've started with here is exporting the chidl component state, and the interface.
  // this referenced under a carousel property on the component state.
  // basically, we are tying the child state to a property on our state.
  const carouselLens = {
    get: ({carousel}: State) => carousel,
    set: (state: any, childState: any) => ({
      carousel: childState
    })
  }

	const CarouselComponent = isolate(Carousel, {onion: carouselLens})(sources)
	const carouselComponentDom$ = CarouselComponent.DOM
  const carouselReducer$: Stream<Reducer<any>> = CarouselComponent.onion

  const contentReducer$ =
    xs.merge(
      defaultReducer$,
      carouselReducer$,
    )

  const contentDom$: Stream<VNode> =
    sources.History
      .map(history => {
        return history.pathname ?
          carouselComponentDom$ : exampleComponentDom$
      })
      .flatten()
      .map(dom =>
        div('.content', [
          dom,
        ]))

	return {
		DOM: contentDom$,
		onion: contentReducer$,
	}
}

export default Content
