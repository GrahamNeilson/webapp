import xs, { Stream } from 'xstream'
import { VNode, div, article, section, h2 } from '@cycle/dom'

import Search from '../../common/search'

interface Sinks {
	DOM: Stream<VNode>
}

function ExampleContent(): Sinks {
  // the carousel may or may not wrap this main content

  const SearchComponent = Search()
  const searchComponentDom$ = SearchComponent.DOM

	const exampleContentDom$ =
    searchComponentDom$.map(searchComponentDom =>
      div('.exampleContent', [
        searchComponentDom,
        article('.article', [
          section('.section', [
            div('.header', [
              h2('.heading', 'Slot con Jackpot')
            ])
          ]),
          section('.section', [
            div('.header', [
              h2('.heading', 'Slot con Jackpot')
            ])
          ]),
          section('.section', [
            div('.header', [
              h2('.heading', 'Slot con Jackpot')
            ])
          ]),
        ]
        )
      ])
    )

	return {
		DOM: exampleContentDom$,
	}
}

export default ExampleContent
