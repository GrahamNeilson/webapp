// todo gradient can be object assign'd in across all.

const slidesData: any[] = [
  {
    title: 'Default',
    image: {
      base: 'https://meetings.skift.com/wp-content/uploads/2022/10/ameer-basheer-4xKm7qT_RMM-unsplash-1-scaled.jpg',
      large:
        'https://meetings.skift.com/wp-content/uploads/2022/10/ameer-basheer-4xKm7qT_RMM-unsplash-1-scaled.jpg',
    },
    html: {
      base: '<a class="button secondary"><span>Registrati Ora</span></a>',
      medium: '<p>Panel Content 1</p>',
    },
  },
  {
    title: 'Upgradium',
    image: {
      base: 'https://res.klook.com/image/upload/Mobile/City/u9idkzri06wom3uuuus2.jpg',
      large:
        'https://res.klook.com/image/upload/Mobile/City/u9idkzri06wom3uuuus2.jpg',
    },
    html: {
      base: '<a class="button secondary"><span>Gioca Ora</span></a>',
      medium: '<p>Panel Content 2</p>',
    },
  },
  {
    title: 'Heart of the Jungle',
    image: {
      base: 'https://www.visittheusa.de/sites/default/files/styles/16_9_1280x720/public/images/hero_media_image/2018-05/182788abfb149bf8f7a25d9e9a803fbe.jpeg?itok=VYl8QPG4',
      large:
        'https://www.visittheusa.de/sites/default/files/styles/16_9_1280x720/public/images/hero_media_image/2018-05/182788abfb149bf8f7a25d9e9a803fbe.jpeg?itok=VYl8QPG4',
    },
    html: {
      base: '<a class="button secondary"><span>Gioca Ora 2</span></a>',
      medium: '<p>Panel Content 3</p>',
    },
  },
  {
    title: 'Miss Fortune',
    image: {
      base: 'https://funancial.news/wp-content/uploads/2022/03/1648733432_Las-Vegas-Strip-Lands-Huge-Sporting-Event-Move-Over-Super.jpg',
      large:
        'https://funancial.news/wp-content/uploads/2022/03/1648733432_Las-Vegas-Strip-Lands-Huge-Sporting-Event-Move-Over-Super.jpg',
    },
    html: {
      base: '<a class="button secondary"><span>Gioca Ora 3</span></a>',
      medium: '<p>Panel Content 4</p>',
    },
  },
];

export default slidesData;
