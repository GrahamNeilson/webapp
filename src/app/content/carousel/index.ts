import xs, { Stream } from 'xstream'
import { div, VNode, nav, button, ul, li, DOMSource, h2, span } from '@cycle/dom'
import { StateSource, Reducer } from 'cycle-onionify'
import { WindowSize } from '../../../drivers/windowSize'

import Icons from '../../utils/icons'
import slidesData from './data'

export interface State {
  slideIndex: number,
  content: VNode,
}

interface Sinks {
	DOM: Stream<VNode>,
	onion: Stream<Reducer<State>>
}

interface Sources {
	DOM: DOMSource
	onion: StateSource<State>
	WindowSize: Stream<WindowSize>
}

function CarouselState(sources: Sources): Sinks {
	const state$ = sources.onion.state$

	// map a click on goToSlide button to it's slide offset from current.
	const goToSlideClick$: Stream<number> =
		sources.DOM
			.select('.goToSlide')
			.events('click')
			.map(event => {
				const target: any = event.target // todo fix type
				return parseInt(target['dataset'].dataSlideIndex)
			})

	// map click on next to +1, previosu to -1
	const nextClick$: Stream<MouseEvent> = sources.DOM.select('.next').events('click')
	const prevClick$: Stream<MouseEvent> = sources.DOM.select('.prev').events('click')

	// any click on those will reset the timer, so let's create that stream of clicks.
	const userInteraction$: Stream<any> =
		xs.merge(
			goToSlideClick$,
			nextClick$,
			prevClick$,
		)

	// every 3 seconds, emit +1, restart the timer whenever the domInitiatedOffset stream emits
	// ie cancel when some user interation occurs.
	const timeInitiatedMove$: Stream<number> =
		userInteraction$
			.startWith(0)
			.map(() => xs.periodic(5000))
			.flatten() // switch latest, in terms of flattening strategies

  // default reducer - content may be (should be) passed in, but we need a default for that.
  // we also need to stor ethe slide index
  const defaultReducer$: Stream<Reducer<State>> =
		xs.of(function initialReducer(prev: State): State {
			if (typeof prev === 'undefined') {
				return {
          content: div('Loading content'),
				  slideIndex: 0,
         }
			} else {
				return prev
      }
		})

	// add one reducer
	const addOneReducer$: Stream<Reducer<State>> =
		xs.merge(
			nextClick$,
			timeInitiatedMove$,
		).map(() => function addOneReducer(prev: State): State {
			return {
        ...prev,
				slideIndex: prev.slideIndex + 1 === slidesData.length ?
					0 : prev.slideIndex + 1
			}
		})

	// subtract one reducer
	const subtractOneReducer$: Stream<Reducer<State>> =
		xs.merge(
			prevClick$,
		).map(() => function subtractOneReducer(prev: State): State {
			return {
        ...prev,
				slideIndex: prev.slideIndex - 1 < 0 ?
					slidesData.length - 1  : prev.slideIndex - 1
			}
		})

	// goto slide reducer
	const goToSlideReducer$: Stream<Reducer<State>> =
		xs.merge(
			goToSlideClick$,
		).map(slideIndex => function goToSlideReducer(prev: State): State {
			return {
        ...prev,
				slideIndex,
			}
		})

	const carouselReducer$ =
		xs.merge(
			defaultReducer$,
			addOneReducer$,
			subtractOneReducer$,
			goToSlideReducer$,
		)

	// map the window size to a data key
	const imageSize$ =
		sources.WindowSize.map(windowSize => windowSize.isBase ? 'base' : 'large')

	const contentSize$ =
		sources.WindowSize.map(windowSize => windowSize.isBase ? 'base' : 'medium')

	// the animations work on entry & exit - one slide div is displayed at a given time
  const vdom$: Stream<VNode> =
      xs.combine(
        state$,
				imageSize$,
				contentSize$,
      ).map(([{slideIndex, content}, imageSize, contentSize]) =>
				div('.carousel', {
          style: {
						backgroundImage: `url(${slidesData[slideIndex].image[imageSize]})`
          }
        }, [
					div('.slides', [
						...slidesData
							.map((slide, index) =>
								index !== slideIndex ? undefined :
									div(`.slide .slide-${index}`, {
										style: {
                      // basic formatting
											transition: 'opacity 300ms',
											opacity: 0.01,
											delayed: {
												opacity: 1,
											},
											remove: {
												opacity: 0,
											}
											// end animation
										},
										props: {
											innerHTML: slidesData[slideIndex].html ? slidesData[slideIndex].html[contentSize] : undefined
										}
									}, slide)
							),
					]),
					nav([
            button('.prev .button .transparent',
              span('.icon', Icons['chevronRight'])
            ),
						ul('.list',
							slidesData.map((slide, index) =>
								li('.listItem',
									button(`.goToSlide`, {
										attrs: {
											disabled: slideIndex === index,
										},
										dataset: {
											dataSlideIndex: index.toString()
                    },
                    class:{
                      'current': slideIndex === index,
                    }
									}, '')
								)
							),
						),
            button('.next .button  .transparent',
              span('.icon', Icons['chevronRight'])
            ),
          ]),
          div('.carouselWrappedContent',
            content,
          )
				])
			)

	return {
		DOM: vdom$,
		onion: carouselReducer$,
	}
}

export default CarouselState
