import * as responsibleImage from './images/responsible.svg'
import * as aamsImage from './images/aams.svg'
import * as eighteenImage from './images/18.png'

export const probabilityUrl = '//assistenza.skybet.it/compliance/probability'
export const admProbabilityUrl = 'https://www.adm.gov.it/portale/monopoli/giochi/probabilita-vincita'

export const complianceImageLinks = [{
  title: 'Gioco Responsabile',
  href: 'https://www.adm.gov.it/portale/monopoli/giochi/normativa/gioco-legale-e-responsabile',
  target: '_blank',
  src: responsibleImage,
}, {
  title: 'Agenzia delle dogane e dei Monopoli',
  href: 'https://www.adm.gov.it/portale/',
  target: '_blank',
  src: aamsImage,
}, {
  title: 'Verifica dell’età e Tutela dei Minori',
  href: '//assistenza.skybet.it/compliance/tutela-minori?consumer=casino',
  target: '_blank',
  src: eighteenImage,
}]
