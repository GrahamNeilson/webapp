import xs, { Stream } from 'xstream'
import { div, VNode, p, ul } from '@cycle/dom'


import {renderImageLink, renderListItem} from '../../utils/render'
import { probabilityUrl, admProbabilityUrl, complianceImageLinks } from './data'

interface Sinks {
	DOM: Stream<VNode>
}

export default function Compliance(): Sinks {
	return {
		DOM: xs.of(
      div('.compliance', [
        p('.complianceText', {
          props: {
            innerHTML: `
              <p>
                Il Gioco è vietato ai minori e può creare dipendenza patologica.
                Consulta le probabilità di vincita
                <a class="link" href="${probabilityUrl}" target="_blank">
                  qui
                </a> e
                <a class="link" href="${admProbabilityUrl}" target="_blank" >
                  sul sito ADM
                </a>
              </p>`
          }
        }),
        div('.complianceLinks', [
          ul('.list',
            complianceImageLinks.map(imageLink => renderListItem(renderImageLink(imageLink)))
          )
        ])
      ])
		)
	}
}
