import xs, { Stream } from 'xstream'
import { div, VNode, input, span, label } from '@cycle/dom'

import Icons from '../../utils/icons'

interface Sinks {
	DOM: Stream<VNode>
}

export default function Search(): Sinks {
	return {
		DOM: xs.of(
			div('.search', [
				div('.inputContainer', [
					span('.icon', Icons['search']),
					label('.label', {
						attrs: {
							for: 'searchterm'
						}
					}, 'Search term'),
					input('.input', {
						attrs: {
							type: 'text',
							id: 'searchterm',
							placeholder: 'Cerca un gioco...'
						},
					}),
					span('.icon .resetSearch', Icons['cross']),
					div('.searchResults', 'Some results...'),
				]),
      ])
		)
	}
}
