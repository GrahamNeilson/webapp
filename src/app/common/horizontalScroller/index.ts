import xs, { Stream } from 'xstream';
import { div, VNode, DOMSource, span, a } from '@cycle/dom';
import { debounce, dropRepeats } from '../../xstream.extra';
import Icons from '../../utils/icons';
import { WindowSize } from '../../../drivers/windowSize';
import Actions, { HostDomAction } from '../../../drivers/hostDom.actions';

function elementScrollWidth$(
  domSource: DOMSource,
  className: string
): Stream<number> {
  return domSource
    .select(className)
    .elements()
    .compose(debounce(50))
    .map((elements: Element[]) => {
      const element = elements[0];
      return element ? element.scrollWidth : 0;
    })
    .compose(dropRepeats());
}

interface Sinks {
  DOM: Stream<VNode>;
  HostDomAction: Stream<HostDomAction>;
}

interface Sources {
  content$: Stream<VNode>;
  DOM: DOMSource;
  WindowSize: Stream<WindowSize>;
}

export default function HorizontalScroller(sources: Sources): Sinks {
  const content$ = sources.content$;
  const containerWidth$ = elementScrollWidth$(
    sources.DOM,
    '.horizontalScroller'
  );
  const contentWidth$ = elementScrollWidth$(
    sources.DOM,
    '.horizontalScrollerInner'
  );
  const isMobileDevice$ = sources.WindowSize.map(
    ({ isMobileDevice }) => isMobileDevice
  );
  const scrollLeft$ = sources.DOM.select('.scrollLeft')
    .events('click')
    .mapTo(-1);
  const scrollRight$ = sources.DOM.select('.scrollRight')
    .events('click')
    .mapTo(1);
  const scrollDirection$ = xs.merge(scrollLeft$, scrollRight$);

  const requireScroll$ = xs
    .combine(containerWidth$, contentWidth$)
    .map(([containerWidth, contentWidth]) => containerWidth < contentWidth);

  const showNav$ = xs
    .combine(isMobileDevice$, requireScroll$)
    .map(([isMobileDevice, requireScroll]) => !isMobileDevice && requireScroll);

  const horizontalScroll$ = sources.DOM.select(
    '.horizontalScrollerInner'
  ).events('scroll');

  const isScrolledRight$ = horizontalScroll$
    .compose(debounce(50))
    .map((event: any) => event.target.scrollLeft > 0)
    .startWith(false);

  const canScrollRight$ = horizontalScroll$
    .compose(debounce(50))
    .map(
      ({ target }: any) =>
        target.scrollLeft + target.offsetWidth < target.scrollWidth - 10
    )
    .startWith(true);

  const adjustScrollPosition$: Stream<HostDomAction> = scrollDirection$
    .map(
      (
        scrollDirection // 1 or -1, right or left
      ) =>
        sources.DOM.select('.horizontalScrollerInner')
          .elements()
          .map((elements: Element[]) => elements[0])
          .filter((element) => !!element) // element not found catch
          .map((element) => {
            // quick testing shows we can set this to below zero and above the max allowed.
            // may require limiting to these bounds on some browsers subject to manual testing.
            const moveBy = element.clientWidth * 0.95 * scrollDirection;
            const scrollLeft = element.scrollLeft + moveBy;

            return {
              type: Actions.SET_ELEMENT_PROPERTY,
              payload: {
                element,
                property: 'scrollLeft',
                value: scrollLeft,
              },
            };
          })
          .take(1)
    )
    .flatten();

  return {
    HostDomAction: adjustScrollPosition$,
    DOM: xs
      .combine(
        content$,
        requireScroll$,
        showNav$,
        isScrolledRight$,
        canScrollRight$
      )
      .map(
        ([content, requireScroll, showNav, isScrolledRight, canScrollRight]) =>
          div('.horizontalScroller', [
            showNav
              ? a(
                  '.button .transparent .scrollLeft',
                  {
                    class: {
                      disabled: !isScrolledRight,
                    },
                  },
                  span('.icon', Icons['chevronRight'])
                )
              : undefined,
            div(
              '.horizontalScrollerWrapper',
              {
                class: {
                  on: requireScroll,
                },
              },
              [
                requireScroll ? div('.horizontalScrollerSignifier') : undefined,
                div('.horizontalScrollerInner', content),
                requireScroll ? div('.horizontalScrollerSignifier') : undefined,
              ]
            ),
            showNav
              ? a(
                  '.button .transparent .scrollRight',
                  {
                    class: {
                      disabled: !canScrollRight,
                    },
                  },
                  span('.icon', Icons['chevronRight'])
                )
              : undefined,
          ])
      ),
  };
}
