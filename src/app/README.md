account state is required at app level as is required fo rthe header (a/c) and content (whether games are playable)

for now, account has the shape:
```
{
	account: {
		user: {
			id: 123,
			displayName: 'Graham',
			balance: {
				real: {
					total: 123
				},
				play: {
					total: 456
				},
			}
		}
	}
}
```

or ```undefined```


