import xs, { Stream } from "xstream";
import { div, VNode, DOMSource } from "@cycle/dom";

import ComplianceHeader from "./complianceHeader";
import Head from "./head";
import Content from "./content";
import NavBar from "./navBar";

import { Reducer, StateSource } from "cycle-onionify";
import { Location } from "history";
const getFooter = () => import("./footer");
import { WindowSize } from "../drivers/windowSize";
import { HostDomAction } from "../drivers/hostDom.actions";
import isolate from "@cycle/isolate";
import { IStorage } from "../drivers/storage";

interface State {
  account: any;
}

interface Sinks {
  DOM: Stream<VNode>;
  History: Stream<string>;
  onion: Stream<Reducer<State>>;
  HostDomAction: Stream<HostDomAction>;
}

interface Sources {
  DOM: DOMSource;
  History: Stream<Location>;
  onion: StateSource<State>;
  WindowSize: Stream<WindowSize>;
  Storage: Stream<IStorage>;
}

function App(sources: Sources): Sinks {
  const state$ = sources.onion.state$;

  const scrollDown$ = sources.DOM.select("document").events("scroll");

  // reducers
  const defaultReducer$: Stream<Reducer<State>> = xs.of(function() {
    return {
      account: {
        user: undefined
      }
    };
  });

  // when we recieve a local storage item, map that toa function that runs against state
  // pluck what we want from storage to overwrite the default state.
  const storageReducer$ = sources.Storage.map((storage: any) => {
    return function(prev: State) {
      return Object.assign(
        { ...prev },
        {
          account: {
            user: storage.user ? JSON.parse(storage.user) : undefined
          }
        }
      );
    };
  });

  // child components
  const ComplianceHeaderComponent = ComplianceHeader();
  const complianceHeaderDom$ = ComplianceHeaderComponent.DOM;

  const HeadComponent = Head(sources);
  const headDom$ = HeadComponent.DOM;
  const headHistory$ = HeadComponent.History;
  const headHostDomAction$ = HeadComponent.HostDomAction;

  const NavBarComponent = NavBar(sources);
  const navBarDom$ = NavBarComponent.DOM;
  const navBarHistory$ = NavBarComponent.History;
  const navBarHostDomAction$ = NavBarComponent.HostDomAction;

  const ContentComponent = isolate(Content)(sources);
  const contentDom$: Stream<VNode> = ContentComponent.DOM;
  const contentReducer$: Stream<Reducer<any>> = ContentComponent.onion;

  const footerDom$: Stream<VNode> = xs
    .merge(scrollDown$, xs.periodic(3000).take(1))
    .take(1)
    .map(() =>
      xs
        .fromPromise(getFooter())
        .map(footerModule => {
          const FooterComponent = footerModule.default();
          const footerDom$ = FooterComponent.DOM;
          return footerDom$;
        })
        .flatten()
    )
    .flatten()
    .startWith(div());

  const appHostDomAction$ = xs.merge(headHostDomAction$, navBarHostDomAction$);

  const appHistory$ = xs.merge(headHistory$, navBarHistory$);

  const appReducer$ = xs.merge(
    defaultReducer$,
    storageReducer$,
    contentReducer$
  );

  const vdom$ = xs
    .combine(
      complianceHeaderDom$,
      headDom$,
      navBarDom$,
      contentDom$,
      footerDom$,
      state$.map(s => div(JSON.stringify(s)))
    )
    .map(
      ([
        complianceHeaderDom,
        headDom,
        navBarDom,
        contentDom,
        footerDom,
        state
      ]) =>
        div(".app", [
          complianceHeaderDom,
          headDom,
          navBarDom,
          contentDom,
          footerDom,
          state
        ])
    );

  return {
    DOM: vdom$,
    History: appHistory$,
    onion: appReducer$,
    HostDomAction: appHostDomAction$
  };
}

export default App;
