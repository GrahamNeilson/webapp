import xs, { Stream } from 'xstream'
import { header, VNode, DOMSource } from '@cycle/dom'

import Logo from './logo'
import Account from './account'
import PrimaryNav from './primaryNav'
import { Location } from 'history'
import { StateSource } from 'cycle-onionify';
import { HostDomAction } from '../../drivers/hostDom.actions';

// what to do, exactly, with account state interfaces?
// account component is responsible for requests to set it, to delete it.
// app is inistialised fron local storage with account state or not.
// it belongs to the app to pass about if it has it.
interface State {
	account: any
}

interface Sources {
	DOM: DOMSource,
	History: Stream<Location>
	onion: StateSource<State>
}

interface Sinks {
  DOM: Stream<VNode>
	History: Stream<string>
	HostDomAction: Stream<HostDomAction>,
}

function Header(sources: Sources): Sinks {
  const LogoComponent = Logo(sources)
  const logoDom$ = LogoComponent.DOM
  const logoHistory$ = LogoComponent.History

  const AccountComponent = Account(sources)
	const accountDom$ = AccountComponent.DOM
	const accountHostDomAction$ = AccountComponent.HostDomAction

	const PrimaryNavComponent = PrimaryNav()
	const primaryNavDom$ = PrimaryNavComponent.DOM

  // create our sinks - merge/combine child sinks as required
	const headerHistory$ =
		xs.merge(
			logoHistory$,
		)

  const headerDom$ =
    xs.combine(
      logoDom$,
			accountDom$,
			primaryNavDom$,
    ).map(([logoDom, headerDom, primaryNavDom]) =>
      header('.head', [
        logoDom,
				headerDom,
				primaryNavDom,
      ])
    )

	return {
    History: headerHistory$,
		DOM: headerDom$,
		HostDomAction: accountHostDomAction$,
	}
}

export default Header
