import xs, { Stream } from 'xstream'
import {div, VNode, p, DOMSource } from '@cycle/dom'
import { StateSource } from 'cycle-onionify'
import Actions, { HostDomAction } from '../../../../drivers/hostDom.actions'

interface Balance {
	total: number
}

interface User {
	id: number
	displayName: string,
	balance: {
		real: Balance
		play: Balance
	}
}

interface State {
	account: {
		user: User
	}
}

interface Sources {
	DOM: DOMSource
	onion: StateSource<State>
}

interface Sinks {
	DOM: Stream<VNode>
	HostDomAction: Stream<HostDomAction>
}

export default function UserDetails(sources: Sources): Sinks {
	// have a look at the cycle catalog version with better entries.
	// the expanded details are not exiting the dom,

	const state$ = sources.onion.state$

	const userDetailsClick$ = sources.DOM.select('.userDetailsInner').events('click')
	const overlayClick$ = sources.DOM.select('.overlay').events('click')

	const open$ = userDetailsClick$
	const closed$ = overlayClick$

	const showDetails$ =
		xs.merge(
			open$.mapTo(true),
			closed$.mapTo(false),
		).startWith(false)

	const disableScroll$ = open$.mapTo({
		type: Actions.DISABLE_SCROLLING
	})

	const enableScroll$ = closed$.mapTo({
		type: Actions.ENABLE_SCROLLING
	})

	const hostDomAction$ =
		xs.merge(
			disableScroll$,
			enableScroll$,
		)

	const vdom$ =
		xs.combine(
			state$,
			showDetails$,
		).map(([state, showDetails]) =>
			div([
				div('.userDetails', {
					class: {
						'on': showDetails ? true : false
					}
				}, [
					div('.userDetailsInner', [
						p(state.account.user.displayName),
						p(`€${state.account.user.balance.real}`),
					]),
					// needs to enter to animate in nicely.
					showDetails!! ? div('.expandedDetails', {
						style: {
							transition: 'all 200ms ease-in-out',
							opacity: 0.01,
							zIndex: 97,
							delayed: {
								opacity: 1,
							},
							remove: {
								opacity: 0.01,
							}
						}
					}, 'expanded...') : div(),
				]),
				showDetails ? div('.overlay', {
					style: {
						transition: 'opacity 200ms ease-in-out',
						width: '100%',
						cursor: 'pointer',
						opacity: 0.01,
						zIndex: 97,
						delayed: {
							opacity: 0.72
						},
						remove: {
							opacity: 0.01
						}
					}
				}) : undefined,
			]),
		)

	return {
		DOM: vdom$,
		HostDomAction: hostDomAction$,
	}
}
