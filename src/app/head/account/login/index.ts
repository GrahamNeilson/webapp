import xs, { Stream } from 'xstream'
import {div, a, span, VNode } from '@cycle/dom'

interface Sinks {
  DOM: Stream<VNode>
}

export default function Login(): Sinks {
	return {
		DOM: xs.of(
			div('.login', [
        a('.link', {
          attrs: {
            title: 'login'
          },
				}, span('Accedi')),
				a('.button .secondary', {
					attrs: {
            title: 'login'
					},
				}, span('Registrati'))
      ])
		)
	}
}
