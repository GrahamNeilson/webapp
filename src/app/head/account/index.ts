import xs, { Stream } from 'xstream'
import {div, a, span, VNode, DOMSource } from '@cycle/dom'
import { StateSource } from 'cycle-onionify'

import Login from './Login'
import UserDetails from './userDetails'
import { HostDomAction } from '../../../drivers/hostDom.actions'

interface Balance {
	total: number
}

interface User {
	id: number
	displayName: string,
	balance: {
		real: Balance
		play: Balance
	}
}

interface State {
	account: {
		user: User
	}
}

interface Sources {
	onion: StateSource<State>
	DOM: DOMSource,
}

interface Sinks {
	DOM: Stream<VNode>
	HostDomAction: Stream<HostDomAction>
}

export default function Account(sources: Sources): Sinks {
	const state$ = sources.onion.state$

	const LoginComponent = Login()
	const loginDom$ = LoginComponent.DOM

	const UserDetailsComponent = UserDetails(sources)
	const userDetailsDom$ = UserDetailsComponent.DOM
	const userDetailsHostDomAction$ = UserDetailsComponent.HostDomAction

	const accountHostDomAction$ = userDetailsHostDomAction$

	const accountDom$ =
		state$.map(state =>
			state.account.user ? userDetailsDom$ : loginDom$
		)
		.flatten()
		.map(dom=>
			div('.account', dom)
		)

	return {
		DOM: accountDom$,
		HostDomAction: accountHostDomAction$,
	}
}
