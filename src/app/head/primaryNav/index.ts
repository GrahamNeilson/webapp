import { nav, ul, li, a, span } from '@cycle/dom'
import xs from 'xstream'
import { primaryMenuItems} from '../../utils/menu'
import { Link } from '../../utils/render';

export default function PrimaryNavigation() {

	const selectedPrimaryMenuItemKey = 'casino'

	return {
		DOM: xs.combine(
			xs.of(primaryMenuItems),
			xs.of(selectedPrimaryMenuItemKey),
		)
			.map(([menuItems, selectedPrimaryMeuItemKey]) =>
				nav('.primaryNav', [
					ul('.list .inline',
						menuItems
							.map((menuItem: any) =>
								// sanitise, enrich, transform data as we need a selected prop.
								// happiest with enrichMenuItem() at the mo for fn naming
								// well in saying that, it's transforming a menu item to link
								// in saying that, aren't menu items always links?!
								// a menu is an array of links.
								// anyway move to intent / or model? yeah, move soemwhere before transforming to a view
								Object.assign({}, menuItem, {
									selected: selectedPrimaryMeuItemKey === menuItem.key
								})
							)
							.map(({ href, title, selected }: Link) =>
								li('.listItem',
									// use renderLink() etc
									a('.link', {
										class: {
											'selected': selected,
										},
										attrs: {
											target: selected ? false : '_blank',
											href: selected ? false : href,
											rel: selected ? false : 'noopener',
											title: title
										},
									}, span(title)) // span is for styling the underline
								),
						)
					)
				])
			)
	}
}
