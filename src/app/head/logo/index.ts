import xs, { Stream } from 'xstream'
import { a, img, DOMSource, VNode, map } from '@cycle/dom'

import * as logoImage from '../../../assets/images/logo.png'

interface Sources {
	DOM: DOMSource
}

interface Sinks {
  DOM: Stream<VNode>
  History: Stream<string>
}

export default function Logo(sources: Sources): Sinks {

	const logoHistory$ =
		sources.DOM.select('.logo')
      .events('click')
      .map((e: any) => {
        e.preventDefault()
        const target = e.target
        const anchor = target['tagName'] === 'IMG' ? target['parentNode'] : target
        return anchor.getAttribute('href')
      })

  const logoDom$ =
    xs.of(
      a('.logo .link .imageLink', {
        attrs: {
          title: 'skybet.it',
          alt: 'SkyBet.it',
          href: '/?graham',
        },
      }, img({
          attrs: {
            src: logoImage,
            alt: 'skybet.it',
          },
        })
      )
    )

	return {
    DOM: logoDom$,
    History: logoHistory$,
	}
}
