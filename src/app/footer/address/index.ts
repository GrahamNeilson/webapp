import xs, { Stream } from 'xstream'
import { VNode } from '@cycle/dom'

import addressData from './data'
import { renderAddress } from '../../utils/render'

interface Sinks {
	DOM: Stream<VNode>
}

function Address(): Sinks {
	const addressDom$ =
		xs.of(renderAddress(addressData))

	return {
		DOM: addressDom$,
	}
}

export default Address
