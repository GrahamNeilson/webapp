import xs, { Stream } from 'xstream'
import { div, VNode } from '@cycle/dom'

import {renderImage, renderListItem, renderList, renderHeader2} from '../../utils/render'
import paymentMethods from './data'

interface Sinks {
	DOM: Stream<VNode>
}

export default function PaymentMethods(): Sinks {
	return {
		DOM: xs.of(
			div('.paymentMethods', [
				renderHeader2('I tuoi metodi di pagamenti preferiti'),
				renderList(
					paymentMethods.map((paymentMethod) =>
						renderListItem(renderImage(paymentMethod))
					)
				)
			])
		)
	}
}
