import * as mastercard from './images/mastercard.svg'
import * as maestro from './images/maestro.svg'
import * as visa from './images/visa.png'
import * as visaDebit from './images/visa-debit.svg'
import * as postepay from './images/postepay.png'
import * as paypal from './images/paypal.png'

import { Image } from '../../utils/render'

const paymentMethods: Image[] = [{
  alt: 'Mastercard',
  src: mastercard,
}, {
  alt: 'Maestro',
  src: maestro,
}, {
  alt: 'Visa',
  src: visa,
}, {
  alt: 'Visa Debit',
  src: visaDebit,
}, {
  alt: 'Postepay',
  src: postepay,
}, {
  alt: 'PayPal',
  src: paypal,
}]

export default paymentMethods
