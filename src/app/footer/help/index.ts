import xs, { Stream } from 'xstream'
import { div, VNode } from '@cycle/dom'

import {renderLink, renderListItem, renderList} from '../../utils/render'
import helpLinks from './data'

interface Sinks {
	DOM: Stream<VNode>
}

export default function Help(): Sinks {
	return {
		DOM: xs.of(
			div('.help',
				renderList(
					helpLinks.map(link =>
						renderListItem(renderLink(link))
					)
				)
			)
		)
	}
}
