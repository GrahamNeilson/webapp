import {Link} from '../../utils/render'

const footerLinks: Link[] = [{
  title: 'Informativa sulla privacy',
  href: '//assistenza.skybet.it/privacy?consumer=casino',
  target: '_blank',
}, {
  title: 'Assistenza',
  href: '//assistenza.skybet.it/?consumer=casino',
  target: '_blank',
}, {
  title: 'Guida',
  href: '//assistenza.skybet.it/guida?consumer=casino',
  target: '_blank',
}, {
  title: 'Gioco Responsabile',
  href: '//assistenza.skybet.it/guida/gioco-responsabile?consumer=casino',
  target: '_blank',
}, {
  title: 'Affilati',
  href: 'http://affiliati.sbpartner.it/',
	target: '_blank',
	rel: 'noopener noreferrer',
}]

export default footerLinks
