import xs, { Stream } from 'xstream'
import { div, VNode, ul } from '@cycle/dom'

import safetyAndResponsibilityImageLinks from './data'
import {renderImageLink, renderList, renderListItem, renderHeader2} from '../../utils/render'

interface Sinks {
	DOM: Stream<VNode>
}

export default function SafetyAndResponsibility(): Sinks {
	return {
		DOM: xs.of(
      div('.safetyAndResponsibility', [
				renderHeader2('Sicurezza e Responsabilità'),
				renderList(
					safetyAndResponsibilityImageLinks
						.map(imageLink =>
							renderListItem(renderImageLink(imageLink))
						)
					)
      ])
		)
	}
}
