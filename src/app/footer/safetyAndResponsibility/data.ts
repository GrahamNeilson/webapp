import * as logico from './images/logico.svg'
import * as essa from './images/essa.png'
import * as gamblingTherapy from './images/gambling-therapy.png'

const safetyAndResponsibilityImageLinks = [{
  title: 'Logico',
  href: 'http://www.assologico.it/',
  target: '_blank',
	src: logico,
	rel: 'noopener noreferrer', // todo, pick up rel in link render fn
}, {
  title: 'essa',
  href: 'http://www.eu-ssa.org/',
  target: '_blank',
	src: essa,
	rel: 'noopener noreferrer',
}, {
  title: 'Gambling Therapy - Ricevi supporto pratico per il tuo problema con il gioco d\'azzardo',
  href: 'https://www.gamblingtherapy.org/it',
  target: '_blank',
	src: gamblingTherapy,
	rel: 'noopener noreferrer',
}]

export default safetyAndResponsibilityImageLinks
