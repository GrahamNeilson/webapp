import xs, { Stream } from 'xstream'
import { div, VNode } from '@cycle/dom'

import * as eflLogo from './images/efl.png'
import {renderImage, renderHeader2} from '../../utils/render'

interface Sinks {
	DOM: Stream<VNode>
}

export default function Sponsorship(): Sinks {
	return {
		DOM: xs.of(
      div('.sponsorship', [
				renderHeader2('Sponsor ufficiale'),
				renderImage({src: eflLogo, alt: 'SkyBet EFL'})
      ])
		)
	}
}
