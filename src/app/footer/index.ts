import xs, { Stream } from 'xstream';
import { footer, VNode, div } from '@cycle/dom';

interface Sinks {
  DOM: Stream<VNode>;
}

import Compliance from '../common/compliance';
import Address from './address';
import Help from './help';
import SafetyAndResponsibility from './safetyAndResponsibility';
import PaymentMethods from './paymentMethods';
import Sponsorship from './sponsorship';

function Footer(): Sinks {
  const ComplianceComponent = Compliance();
  const complianceDom$ = ComplianceComponent.DOM;

  const AddressComponent = Address();
  const addressDom$ = AddressComponent.DOM;

  const HelpComponent = Help();
  const helpDom$ = HelpComponent.DOM;

  const SafetyAndResponsibilityComponent = SafetyAndResponsibility();
  const safetyAndResponsibilityDom$ = SafetyAndResponsibilityComponent.DOM;

  const PaymentMethodsComponent = PaymentMethods();
  const paymentMethodsDom$ = PaymentMethodsComponent.DOM;

  const SponsorshipComponent = Sponsorship();
  const sponsorshipDom$ = SponsorshipComponent.DOM;

  const footerDom$ = xs
    .combine(
      complianceDom$,
      addressDom$,
      helpDom$,
      safetyAndResponsibilityDom$,
      paymentMethodsDom$,
      sponsorshipDom$
    )
    .map(
      ([
        complianceDom,
        addressDom,
        helpDom,
        safetyAndResponsibilityDom,
        paymentMethodsDom,
        sponsorshipDom,
      ]) =>
        footer('.footer', [
          div([complianceDom, addressDom, helpDom]),
          div([safetyAndResponsibilityDom, paymentMethodsDom, sponsorshipDom]),
        ])
    );

  return {
    DOM: footerDom$,
  };
}

export default Footer;
