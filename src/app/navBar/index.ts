import { div, DOMSource, VNode } from '@cycle/dom'
import xs, { Stream } from 'xstream'
import { Location } from 'history'

import SecondaryNav from './secondaryNav'
import PromotionsLink from './promotionsLink'
import NavBarSearch from './navBarSearch'
import { WindowSize } from '../../drivers/windowSize';
import { HostDomAction } from '../../drivers/hostDom.actions'

interface Sources {
	DOM: DOMSource,
  History: Stream<Location>
  WindowSize: Stream<WindowSize>
}

interface Sinks {
	DOM: Stream<VNode>,
  History: Stream<string>
  HostDomAction: Stream<HostDomAction>
}

export default function NavBar(sources: Sources): Sinks {

  const SecondaryNavComponent = SecondaryNav(sources)
	const secondaryNavDom$ = SecondaryNavComponent.DOM
  const secondaryNavHistory$ = SecondaryNavComponent.History
  const secondaryNavHostDomAction$ = SecondaryNavComponent.HostDomAction

  const PromotionsLinkComponent = PromotionsLink()
	const promotionsLinkDom$ = PromotionsLinkComponent.DOM

	const NavBarSearchComponent = NavBarSearch()
  const navBarSearchDom$ = NavBarSearchComponent.DOM

  const menuDom$ =
    xs.combine(
      secondaryNavDom$,
      promotionsLinkDom$,
      navBarSearchDom$,
    ).map(([secondaryNavDom, promotionsLinkDom, navBarSearchDom]) =>
      div('.navBar', [
        div('.navBarInner', [
          secondaryNavDom,
          promotionsLinkDom,
          navBarSearchDom,
        ])
      ])
    )

  const menuHistory$ =
      xs.merge(
        secondaryNavHistory$
      )

	return {
		DOM: menuDom$,
    History: menuHistory$,
    HostDomAction: secondaryNavHostDomAction$,
	}
}
