import xs, { Stream } from 'xstream'
import { VNode, span, a } from '@cycle/dom'

import Icons from '../../utils/icons'

interface Sinks {
	DOM: Stream<VNode>
}

export default function PromotionsLink(): Sinks {
	return {
		DOM: xs.of(
			a('.link .promotionsLink', [
				span('.icon', Icons['megaphone']),
				span('Promozioni'),
			])
		)
	}
}
