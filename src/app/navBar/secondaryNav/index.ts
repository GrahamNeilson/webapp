import { nav, ul, li, div, a, span, DOMSource, VNode, map } from '@cycle/dom'
import xs, { Stream } from 'xstream'

import Icons from '../../utils/icons'
import { secondaryMenuItems } from '../../utils/menu'
import { Location } from 'history';
import HorizontalScroller from '../../common/horizontalScroller'
import { WindowSize } from '../../../drivers/windowSize';
import { HostDomAction } from '../../../drivers/hostDom.actions';

interface Sources {
	DOM: DOMSource,
  History: Stream<Location>
  WindowSize: Stream<WindowSize>
}

interface Sinks {
	DOM: Stream<VNode>,
  History: Stream<string>
  HostDomAction: Stream<HostDomAction>
}

export default function SecondaryNavigation(sources: Sources): Sinks {
	const defaultSecondaryMenuItemKey = ''

	const history$ =
		sources.DOM.select('.secondary-navigation-link')
			.events('click')
			.map(getDataUrlFromEvent)

	const selectedSecondaryMenuItemKey$ =
		sources.History
			.map(history => history.pathname)
			.map(path => path.split('/')[1])
      .map(segment => segment ? segment : defaultSecondaryMenuItemKey)

  const menu$ =
    xs.combine(
      xs.of(secondaryMenuItems),
      selectedSecondaryMenuItemKey$,
    ).map(([menuItems, selectedSecondaryMenuItemKey]) =>
      ul('.list .inline',
        menuItems
          .map(({ href, title, key, icon }: any) =>
            li('.listItem',
              a('.link', {
                class: {
                  'selected': selectedSecondaryMenuItemKey === key,
                },
                attrs: {
                  title: title
                },
                href: `/${key}`
              }, [
                  span('.icon', Icons[icon]),
                  span(title),
                ])
            ),
        )
      ),
    )

  const HorizontalScrollerComponent = HorizontalScroller({
    content$: menu$,
    DOM: sources.DOM,
    WindowSize: sources.WindowSize
  })
  const horizontalScrollerDom$ = HorizontalScrollerComponent.DOM
  const horizontalScrollerHostDomAction$ = HorizontalScrollerComponent.HostDomAction

  // merge child sinks and send to sinks
  const secondaryNavHostDomAction$ = horizontalScrollerHostDomAction$

  const secondaryNavDom$ =
    horizontalScrollerDom$.map(horizontalScrollerDom =>
      div('.secondaryNav', [
        horizontalScrollerDom,
      ])
    )

	return {
		DOM: secondaryNavDom$,
    History: history$,
    HostDomAction: secondaryNavHostDomAction$,
	}
}

function getDataUrlFromEvent(event: MouseEvent) {
	const target: any = event.target
	const anchor = target['tagName'] === 'SPAN' ? target['parentNode'] : target
	return anchor.getAttribute("href")
}
