import { Stream } from 'xstream'
import { div, VNode } from '@cycle/dom'

import Search from '../../common/search'

interface Sinks {
	DOM: Stream<VNode>
}

export default function NavBarSearch(): Sinks {

  const SearchComponent = Search()
  const searchDom$ = SearchComponent.DOM

  const navBarSearchDom$ =
		searchDom$.map(searchDom =>
      div('.navBarSearch', [
        searchDom,
      ])
    )

	return {
		DOM: navBarSearchDom$,
	}
}

