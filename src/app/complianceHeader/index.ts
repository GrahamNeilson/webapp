import xs, { Stream } from 'xstream'
import { div, VNode } from '@cycle/dom'

import Compliance from '../common/compliance'

interface Sinks {
	DOM: Stream<VNode>
}

export default function ComplianceHeader(): Sinks {

  const ComplianceComponent = Compliance()
  const complianceDom$ = ComplianceComponent.DOM

  const complianceHeaderDom$ =
    complianceDom$.map(complianceDom =>
      div('.complianceHeader', [
        complianceDom
      ])
    )

	return {
		DOM: complianceHeaderDom$,
	}
}

